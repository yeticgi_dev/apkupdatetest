﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using System.IO;

public class ApkDownloader : MonoBehaviour
{
    void Start()
    {
        // DownloadSync();

        UnityEngine.Debug.Log("ApiVersion1 = " + GetSDKLevel());
        UnityEngine.Debug.Log("ApiVersion2 = " + SystemInfo.operatingSystem);
        UnityEngine.Debug.Log("ApiVersion3 = " + int.Parse(SystemInfo.operatingSystem.Substring(SystemInfo.operatingSystem.IndexOf("-") + 1, 3)));


        StartCoroutine(DownloadAndInstallApk("http://saz.yeti-dev.com/sayetiweb01/fakelove/dev/test-apk/app-debug-v2.apk"));
    }

    public int GetSDKLevel()
    {
        AndroidJavaClass versionInfo = new AndroidJavaClass("android.os.Build$VERSION");
        int sdkLevel = versionInfo.GetStatic<int>("SDK_INT");
        return sdkLevel;
    }

    public IEnumerator DownloadAndInstallApk(string url)
    {
        string filename = Path.GetFileName(url);
        string fullPath = Path.Combine(GetDestinationPath(), filename);

        if (File.Exists(fullPath) == false)
        {
            Debug.LogFormat("DownloadAndInstallApk - starting download from '{0}' to '{1}'", url, fullPath);
            yield return StartCoroutine(DownloadApk(url, fullPath));
        }
        else
        {
            Debug.LogFormat("DownloadAndInstallApk - already installed at '{0}'", fullPath);
        }

        InstallApk(fullPath);
    }

    private IEnumerator DownloadApk(string url, string fullPath)
    {
        WWW www = new WWW(url);
        string progress = "0%";
        // yield return www;
        while (!www.isDone)
        {
            progress = (www.progress * 100).ToString() + "%";
            Debug.LogFormat("Download progress {2}, for '{0}' to '{1}'", url, fullPath, progress);
            yield return null;
        }

        if (string.IsNullOrEmpty(www.error) == false)
        {
            Debug.LogErrorFormat("Download FAILED {2}, for '{0}' to '{1}'", url, fullPath, www.error);
            yield break;
        }

        Debug.LogFormat("Download progress {2}, for '{0}' to '{1}'", url, fullPath, progress);

        Debug.LogFormat("Download COMPLETE", url, fullPath, progress);
        Debug.LogFormat("Download writing...", url, fullPath, progress);
        File.WriteAllBytes(fullPath, www.bytes);
        Debug.LogFormat("Download Write complete", url, fullPath, progress);
        Debug.LogFormat("Download Test: {0}", File.Exists(fullPath));
    }

    private bool InstallApk(string apkPath)
    {
        bool success = true;
        // GameObject.Find("TextDebug").GetComponent<Text>().text = "Installing App";
        Debug.LogFormat("Install - Installing...");

        try
        {
            int androidApiLevel = GetSDKLevel();
            Debug.LogFormat("Install - Android SDK_INT - " + androidApiLevel);

            if (androidApiLevel >= 24) // is Android 7.0 or more
            {
                Debug.LogFormat("Install - taking the FileProvider path");

                //Get Activity then Context
                AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
                AndroidJavaObject unityContext = currentActivity.Call<AndroidJavaObject>("getApplicationContext");

                //Get the package Name
                string packageName = unityContext.Call<string>("getPackageName");
                string authority = packageName + ".fileprovider";

                AndroidJavaClass intentObj = new AndroidJavaClass("android.content.Intent");
                string ACTION_VIEW = intentObj.GetStatic<string>("ACTION_VIEW");
                AndroidJavaObject intent = new AndroidJavaObject("android.content.Intent", ACTION_VIEW);


                int FLAG_ACTIVITY_NEW_TASK = intentObj.GetStatic<int>("FLAG_ACTIVITY_NEW_TASK");
                int FLAG_GRANT_READ_URI_PERMISSION = intentObj.GetStatic<int>("FLAG_GRANT_READ_URI_PERMISSION");

                //File fileObj = new File(String pathname);
                AndroidJavaObject fileObj = new AndroidJavaObject("java.io.File", apkPath);
                //FileProvider object that will be used to call it static function
                AndroidJavaClass fileProvider = new AndroidJavaClass("android.support.v4.content.FileProvider");
                //getUriForFile(Context context, String authority, File file)
                AndroidJavaObject uri = fileProvider.CallStatic<AndroidJavaObject>("getUriForFile", unityContext, authority, fileObj);

                intent.Call<AndroidJavaObject>("setDataAndType", uri, "application/vnd.android.package-archive");
                intent.Call<AndroidJavaObject>("addFlags", FLAG_ACTIVITY_NEW_TASK);
                intent.Call<AndroidJavaObject>("addFlags", FLAG_GRANT_READ_URI_PERMISSION);
                currentActivity.Call("startActivity", intent);
            }
            else
            {
                Debug.LogFormat("Install - taking the File path");

                AndroidJavaClass intentObj = new AndroidJavaClass("android.content.Intent");
                string ACTION_VIEW = intentObj.GetStatic<string>("ACTION_VIEW");
                int FLAG_ACTIVITY_NEW_TASK = intentObj.GetStatic<int>("FLAG_ACTIVITY_NEW_TASK");
                AndroidJavaObject intent = new AndroidJavaObject("android.content.Intent", ACTION_VIEW);

                AndroidJavaObject fileObj = new AndroidJavaObject("java.io.File", apkPath);
                AndroidJavaClass uriObj = new AndroidJavaClass("android.net.Uri");
                AndroidJavaObject uri = uriObj.CallStatic<AndroidJavaObject>("fromFile", fileObj);

                intent.Call<AndroidJavaObject>("setDataAndType", uri, "application/vnd.android.package-archive");
                intent.Call<AndroidJavaObject>("addFlags", FLAG_ACTIVITY_NEW_TASK);
                intent.Call<AndroidJavaObject>("setClassName", "com.android.packageinstaller", "com.android.packageinstaller.PackageInstallerActivity");

                AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
                currentActivity.Call("startActivity", intent);
            }

            // GameObject.Find("TextDebug").GetComponent<Text>().text = "Success";
            Debug.LogFormat("Install - SUCCESS");
        }
        catch (System.Exception e)
        {
            // GameObject.Find("TextDebug").GetComponent<Text>().text = "Error: " + e.Message;
            Debug.LogFormat("Install - Error: {0}", e.Message);
            success = false;
        }

        return success;
    }



    private void DownloadSync()
    {
        WebClient client = new WebClient();
        string destPath = GetDestinationPath();
        client.DownloadFileAsync(new Uri("http://saz.yeti-dev.com/sayetiweb01/fakelove/dev/test-apk/app-debug-v2.apk"), destPath);
    }



    public string GetDestinationPath()
    {
        string path = "";

#if UNITY_ANDROID && !UNITY_EDITOR
        try
        {
            AndroidJavaClass jc = new AndroidJavaClass("android.os.Environment");
            path = jc.CallStatic<AndroidJavaObject>("getExternalStorageDirectory").Call<string>("getAbsolutePath");
            return path;
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }
#else
        path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData);
#endif

        return null;
    }
}
