﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Test : MonoBehaviour
{
    public ApkUpdateManager manager;

    public Button button;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("here!");
        button.onClick.AddListener(OnButtonClicked);
    }

    void OnButtonClicked() {
        manager.CheckAPKUpdate(new Action<bool>(OnAPKUpdatedChecked));
    }

    void OnAPKUpdatedChecked(bool updateReady) {
        if (updateReady) {
            manager.DownloadAndInstallApk();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
