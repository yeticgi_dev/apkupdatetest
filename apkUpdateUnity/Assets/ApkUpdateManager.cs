﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using System.IO;
using UnityEngine.Networking;

[Serializable]
    public class CloudConfig
    {
        public string latestBuildUrl;
        public int latestVersionCode;
        public bool autoUpdate;
    }

public class ApkUpdateManager : MonoBehaviour
{
    public string configURL = "";
    public bool IsDownloadingApk { get; private set; }
    public float DownloadProgress { get; private set; }

    public CloudConfig cloudConfig;

    /// <summary>
    /// Check to see if an APK update is available.
    /// </summary>
    /// <param name="callback">Invoked when the update check is complete. Bool indicates if update is available or not.</param>
    public void CheckAPKUpdate(System.Action<bool> callback)
    {
        StartCoroutine(CheckAPKUpdateRoutine(callback));
    }

    private IEnumerator CheckAPKUpdateRoutine(System.Action<bool> callback)
    {
        yield return DownloadCloudConfig();
        Debug.LogFormat("[ApkDownloader] Cloud Config Successfully retrieved: {0}", JsonUtility.ToJson(cloudConfig));

        if (cloudConfig.autoUpdate)
        {
            Debug.LogFormat("[ApkDownloader] Checking for APK update...");

#if UNITY_ANDROID && !UNITY_EDITOR
            int versionCode = GetVersionCode();
            Debug.LogFormat("[ApkDownloader] Comparing version codes... this build: {0}, build on cloud: {1}", versionCode, cloudConfig.latestVersionCode);
            callback(cloudConfig.latestVersionCode > versionCode);
#else
            Debug.LogFormat("[ApkDownloader] Skipping Apk Updater because we aren't on Android...");
            callback(false);
#endif
        }
        else
        {
            callback(false);
        }
    }

    public int GetVersionCode()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        AndroidJavaClass contextCls = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject context = contextCls.GetStatic<AndroidJavaObject>("currentActivity"); 
        AndroidJavaObject packageMngr = context.Call<AndroidJavaObject>("getPackageManager");
        string packageName = context.Call<string>("getPackageName");
        AndroidJavaObject packageInfo = packageMngr.Call<AndroidJavaObject>("getPackageInfo", packageName, 0);
        return packageInfo.Get<int>("versionCode");
#endif
        return -1;
    }

    private IEnumerator DownloadCloudConfig()
    {
        Debug.Log("[ApkDownloader] Download cloudconfig from: " + configURL);

        var request = UnityWebRequest.Get(configURL);
        request.redirectLimit = 10;

        yield return request.SendWebRequest();
        if (string.IsNullOrEmpty(request.error) == false)
        {
            Debug.Log("[ApkDownloader] ERROR downloading cloudconfig" + request.downloadHandler.text);
            yield break;
        }

        Debug.Log("[ApkDownloader] Downloaded cloudconfig: " + request.downloadHandler.text);

        // cloudConfig = Utf8Json.JsonSerializer.Deserialize<CloudConfig>(request.downloadHandler.text);
        cloudConfig = JsonUtility.FromJson<CloudConfig>(request.downloadHandler.text);
    }

    void Start()
    {
        // DownloadSync();

        // UnityEngine.Debug.Log("ApiVersion1 = " + GetSDKLevel());
        // UnityEngine.Debug.Log("ApiVersion2 = " + SystemInfo.operatingSystem);
        // UnityEngine.Debug.Log("ApiVersion3 = " + int.Parse(SystemInfo.operatingSystem.Substring(SystemInfo.operatingSystem.IndexOf("-") + 1, 3)));


        // StartCoroutine(DownloadAndInstallApk("http://saz.yeti-dev.com/sayetiweb01/fakelove/dev/test-apk/app-debug-v2.apk"));
    }

    public int GetSDKLevel()
    {
        AndroidJavaClass versionInfo = new AndroidJavaClass("android.os.Build$VERSION");
        int sdkLevel = versionInfo.GetStatic<int>("SDK_INT");
        return sdkLevel;
    }

    // public IEnumerator DownloadAndInstallApk(string url)
    // {
    //     string filename = Path.GetFileName(url);
    //     string fullPath = Path.Combine(GetDestinationPath(), filename);

    //     if (File.Exists(fullPath) == false)
    //     {
    //         Debug.LogFormat("DownloadAndInstallApk - starting download from '{0}' to '{1}'", url, fullPath);
    //         yield return StartCoroutine(DownloadApk(url, fullPath));
    //     }
    //     else
    //     {
    //         Debug.LogFormat("DownloadAndInstallApk - already installed at '{0}'", fullPath);
    //     }

    //     InstallApk(fullPath);
    // }

    public void DownloadAndInstallApk() {
        StartCoroutine(DownloadAPK(cloudConfig.latestBuildUrl));
    }

    
    public IEnumerator DownloadAPK(string url)
    {
        IsDownloadingApk = true;

        string filename = Path.GetFileName(url);


        Debug.LogFormat("[ApkDownloader] Downloading and saving new APK...\nfrom: '{0}', to: '{1}'", url, filename);

        string savePath = Path.Combine(Application.persistentDataPath, "data");
        savePath = Path.Combine(savePath, filename);

        // Dictionary<string, string> header = new Dictionary<string, string>();
        string userAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36";
        // header.Add("User-Agent", userAgent);
        UnityWebRequest www = new UnityWebRequest(url);
        DownloadHandlerBuffer dH = new DownloadHandlerBuffer();
        www.downloadHandler = dH;
        www.SetRequestHeader("User-Agent", userAgent);
        www.SendWebRequest();

        Debug.Log("Gwar?");
        while (!www.isDone)
        {
            //Must yield below/wait for a frame
            Debug.Log("[ApkDownloader] Status: " + www.downloadProgress);

            DownloadProgress = www.downloadProgress;
            yield return null;
        }
        Debug.Log("Buh-huh!");

        DownloadProgress = 1.0f;


        if (string.IsNullOrEmpty(www.error) == false)
        {
            Debug.Log("[ApkDownloader] Error during downloading APK: " + www.error);
            yield break;
        }

        Debug.Log("Jumbo! " + www.error);

        Debug.Log("WWWW: " + www);
        Debug.Log("Handler: " + www.downloadHandler);

        byte[] yourBytes = www.downloadHandler.data;

        Debug.Log("Gimme those bytes! " + yourBytes);

        Debug.Log("[ApkDownloader] Done downloading. Size: " + yourBytes.Length);


        //Create Directory if it does not exist
        if (!Directory.Exists(Path.GetDirectoryName(savePath)))
        {
            Directory.CreateDirectory(Path.GetDirectoryName(savePath));
            Debug.Log("[ApkDownloader] Created Dir");
        }

        try
        {
            //Now Save it
            System.IO.File.WriteAllBytes(savePath, yourBytes);
            Debug.Log("[ApkDownloader] Saved Data to: " + savePath.Replace("/", "\\"));
            Debug.Log("[ApkDownloader] Saved Data");
        }
        catch (Exception e)
        {
            Debug.LogWarning("[ApkDownloader] Failed To Save Data to: " + savePath.Replace("/", "\\"));
            Debug.LogWarning("[ApkDownloader] Error: " + e.Message);
            Debug.Log("[ApkDownloader] Error Saving Data");
            yield break;
        }

        //Install APK
        InstallAPK(savePath);

        IsDownloadingApk = false;
    }

    private bool InstallAPK(string apkPath)
    {
        bool success = true;
        Debug.Log("[ApkDownloader] Installing App");

        try
        {
            Debug.Log("1");
            //Get Activity then Context
            AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaObject unityContext = currentActivity.Call<AndroidJavaObject>("getApplicationContext");
            Debug.Log("2");
            //Get the package Name
            string packageName = unityContext.Call<string>("getPackageName");
            string authority = packageName + ".fileprovider";
            Debug.Log("3 " + apkPath);
            AndroidJavaClass intentObj = new AndroidJavaClass("android.content.Intent");
            string ACTION_VIEW = intentObj.GetStatic<string>("ACTION_VIEW");
            AndroidJavaObject intent = new AndroidJavaObject("android.content.Intent", ACTION_VIEW);
            Debug.Log("4");

            int FLAG_ACTIVITY_NEW_TASK = intentObj.GetStatic<int>("FLAG_ACTIVITY_NEW_TASK");
            int FLAG_GRANT_READ_URI_PERMISSION = intentObj.GetStatic<int>("FLAG_GRANT_READ_URI_PERMISSION");

            //File fileObj = new File(String pathname);
            AndroidJavaObject fileObj = new AndroidJavaObject("java.io.File", apkPath);
            //FileProvider object that will be used to call it static function
            AndroidJavaClass fileProvider = new AndroidJavaClass("android.support.v4.content.FileProvider");
            //getUriForFile(Context context, String authority, File file)
            Debug.Log("4.5");
            AndroidJavaObject uri = fileProvider.CallStatic<AndroidJavaObject>("getUriForFile", unityContext, authority, fileObj);
            Debug.Log("5");
            intent.Call<AndroidJavaObject>("setDataAndType", uri, "application/vnd.android.package-archive");
            intent.Call<AndroidJavaObject>("addFlags", FLAG_ACTIVITY_NEW_TASK);
            intent.Call<AndroidJavaObject>("addFlags", FLAG_GRANT_READ_URI_PERMISSION);
            currentActivity.Call("startActivity", intent);

            Debug.Log("[ApkDownloader] Success");
        }
        catch (System.Exception e)
        {
            Debug.Log("[ApkDownloader] Error: " + e.Message);
            success = false;
        }

        return success;
    }


//     private void DownloadSync()
//     {
//         WebClient client = new WebClient();
//         string destPath = GetDestinationPath();
//         client.DownloadFileAsync(new Uri("http://saz.yeti-dev.com/sayetiweb01/fakelove/dev/test-apk/app-debug-v2.apk"), destPath);
//     }



//     public string GetDestinationPath()
//     {
//         string path = "";

// #if UNITY_ANDROID && !UNITY_EDITOR
//         try
//         {
//             AndroidJavaClass jc = new AndroidJavaClass("android.os.Environment");
//             path = jc.CallStatic<AndroidJavaObject>("getExternalStorageDirectory").Call<string>("getAbsolutePath");
//             return path;
//         }
//         catch (Exception e)
//         {
//             Debug.Log(e.Message);
//         }
// #else
//         path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData);
// #endif

//         return null;
//     }
}
