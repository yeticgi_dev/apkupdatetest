﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivityRunner : MonoBehaviour
{
    void Start()
    {
		StartPackage("com.androidpala.updatetutorial.MainActivity");
    }

    void Update()
    {

    }

    void StartPackage(string package)
    {
		Debug.Log("StartPackage: '" + package + "'");

        AndroidJavaClass activityClass;
        AndroidJavaObject activity, packageManager;
        AndroidJavaObject launch;

        activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

        activity = activityClass.GetStatic<AndroidJavaObject>("currentActivity");
		Debug.Log("[ActivityRunner] activity: '" + activity + "'");
        packageManager = activity.Call<AndroidJavaObject>("getPackageManager");
		Debug.Log("[ActivityRunner] packageManager: '" + packageManager + "'");
        launch = packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage", package);
		Debug.Log("[ActivityRunner] launch: '" + launch + "'");

        activity.Call("startActivity", launch);
		Debug.Log("[ActivityRunner] start3ed");
    }
}
